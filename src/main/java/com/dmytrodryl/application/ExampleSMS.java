package com.dmytrodryl.application;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID="AC4e436a9782fa4c733d9dd04d782e995e";
    public static final String AUTH_TOKEN="3877b503ced9eeedef328cf82319e2c5";

    public static void send(String str){
        Twilio.init(ACCOUNT_SID,AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380733157525"),
                new PhoneNumber("+12134189399"),str).create();
    }
}
